package PT2020.Assignment_2;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Scheduler {

	private List<Coada> cozi = Collections.synchronizedList(new ArrayList<Coada>());
	private int nrCozi;
	private  List<Thread> threads;
	
  private BufferedWriter writer;
  private int maxTime;
	

	public Scheduler(int nrCozi,BufferedWriter w,int time) {
		
		this.nrCozi = nrCozi;
	
		writer = w;
		maxTime = time;
		int i =1;
		
		while(i<=this.nrCozi) {
			Coada c = new Coada(i);
			cozi.add(c);
			i++;
		}
		
		threads = new ArrayList<Thread>();
	}
	
	public Coada selectCoada() { 
		
		int minSize = maxTime;
		Coada minCoada = new Coada();
		int sum ;
		for(Coada c : cozi) {
			sum = 0;
			for(Client client : c.getClients()) {
				sum+=client.getTservice();
			}
			
			if(minSize > sum) {
				
				minSize = sum;
				minCoada = c;
			}
		}		
		return minCoada;
	}
	
	public void insertClient(Client client) {
		
		Coada c = selectCoada();
		boolean isEmpty = false;
		
		if(c.getClients().isEmpty()) {
			isEmpty = true;
		}
		
		c.addClient(client); 
		
		if(isEmpty ) {
		  Thread t = new Thread(c);
		  threads.add(t);
		  t.start();
	      this.killThreads(); //stergem threadurile moarte
		}
			
	}
	
	
	public void killThreads() {
		
		for(int i = 0; i < threads.size(); i++) {
			
			 if(!threads.get(i).isAlive()) {	
				threads.remove(i);				
			}
		}
	}
	
	
	public void afisare() throws IOException {
		
		writer.write(this.toString());
		writer.write("\n");
	}
	

	public void stopThreads() {
		for(Coada c: cozi) {
			c.stopRunning();
		}
	}
	

	public String toString() {
		String mesaj = "";
		for(Coada q : cozi) {
			mesaj = mesaj + q + '\n';
		}
		
		return mesaj;
	}
	


}
