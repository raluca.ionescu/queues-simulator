package PT2020.Assignment_2;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Manager implements Runnable{

	private int time ;
	private int nrCozi;
	private int nrClients;
    static int nr = 1;
    private int maxArrival;
    private int minArrival;
    private int maxService;
    private int minService;
    private int average = 0;
    private BufferedWriter writer;

    private Scheduler scheduler;
	
	private List<Client> clients =  Collections.synchronizedList(new ArrayList<Client>());

	
	public Manager(String arg1, String arg2) throws IOException {  
		
		File file =  new File(arg1).getAbsoluteFile(); 	
		File out = new File(arg2).getAbsoluteFile();
		
		writer = new BufferedWriter(new FileWriter(out));
		
    	Scanner s = null;
		String x1, x2;
		try {
			s=new Scanner(file);
		}catch(Exception e) {
			System.out.println("Could not open/read from file!");
		}
		
		while(s.hasNext()) {
			this.nrClients = s.nextInt();
			this.nrCozi = s.nextInt();
			this.time = s.nextInt();
			x1 = s.next();
			String[] s1 = x1.split(",");
			this.minArrival = Integer.parseInt(s1[0]);
			this.maxArrival = Integer.parseInt(s1[1]);
			x2 = s.next();
			String[] s2= x2.split(",");
			this.minService = Integer.parseInt(s2[0]);
			this.maxService = Integer.parseInt(s2[1]);
		}
			
		this.generateClients();	
		scheduler = new Scheduler(nrCozi, writer, time);

	}
	
	
	public void generateClients() {
		int nr = 1;
		Client c;
		int tarrival;
		int tservice;
		while(nr <= nrClients) {
			
			Random r1 = new Random();		
			tarrival = r1.nextInt(maxArrival - minArrival)+minArrival;
			tservice = r1.nextInt(maxService-minService)+minService;
			c  =  new Client(nr,tarrival, tservice);
			clients.add(c);
			nr++;
		}
		
		Collections.sort(clients);
	}

	public void run() {
		
		int t = 0;
		int i ;
		int wait;
		
		
	try {
		
	while(t <= time) {
		
		writer.write("Time: "+ t + "\n");
	    Thread.sleep(1000);
		for( i = 0;i<clients.size();i++) {
			
			    if(clients.get(i).getTarrival() == t) {
				
				    scheduler.insertClient(clients.get(i));	
				    //in cazul in care clientul nu o sa iasa din coada pana se termina timpul de simulare
				    // calculam timpul petrecut in coada scazand din timpul pe care trebuie sa il petreaca timpul ramas la terminarea simularii
				    if(clients.get(i).getTarrival() + clients.get(i).getWaitingTime() > time) {
				    	
				    	wait = clients.get(i).getTarrival() + clients.get(i).getWaitingTime() -time ;// timpul la care o ajunga cand t = time
				    	average += clients.get(i).getWaitingTime() - wait; // timpul petrecut efectiv in coada , pana la terminarea simularii
				    
				    }else {
				    	average  += clients.get(i).getWaitingTime();
				    	
				    }
				    clients.remove(clients.get(i));
					i--;
					
				}			
	    }
		
		    	   		
		if(clients.isEmpty()) {
			writer.write("No waiting clients"+ "\n");	
		}
		else {
			
			String mesaj = "Waiting clients: ";
			int s = 0;
			for(Client c : clients) {
				mesaj += c + " ";
				s++;
				if(s== 10) {
					mesaj +="\n";
					s = 0;
				}
			}
		     writer.write(mesaj+"\n");
		     }
		
		scheduler.afisare();
		
		t++;
	}
	
		Thread.sleep(1000);
		
		scheduler.stopThreads(); // oprim toate threadurile 
	
		writer.write("Average waiting time: "+(float)average/nrClients );
		
	    writer.close();
	    
		}catch (InterruptedException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	
	}

	public static void main(String[] args) {
		
		Manager manager;

		try {
			manager = new Manager(args[0], args[1]);
			Thread t = new Thread(manager);
			t.start();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	
		
	}

}