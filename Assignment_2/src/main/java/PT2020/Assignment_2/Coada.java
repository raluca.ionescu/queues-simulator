package PT2020.Assignment_2;


import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class Coada implements Runnable {
	
	private int nr;

	private LinkedBlockingQueue<Client> clients;
	private AtomicInteger waitingPeriod ;
	private  AtomicBoolean running ;
	
	
    public Coada(int nr) {
    	this.nr = nr;
    	 clients = new LinkedBlockingQueue<Client>();
    	 waitingPeriod = new AtomicInteger(0);
    	 running = new AtomicBoolean(false);
    }
    
    public Coada() {
    	
    	 clients = new LinkedBlockingQueue<Client>();
    	 waitingPeriod = new AtomicInteger(0);
    	 
    }
    
    public  void addClient(Client c) {
    	
    	this.startRunning();
    	c.setWaitingTime(waitingPeriod.get());
    	clients.add(c);
    	waitingPeriod.addAndGet(c.getTservice());
    }
    
    public void removeClient(Client c) {
    	clients.remove(c);
    }
    
    public int getTimeService() { 
    	int s = 0;
    	for(Client c : clients) {
    		s += c.getTservice();
    	}
    	return s;
    }
    
    public void run() {
		
    	while(running.get()) {  
    
			if(!clients.isEmpty()) {		
				processClient();
			}
		}
	}
    
    
    public AtomicInteger getWaitingPeriod() {
		return waitingPeriod;
	}

	public void setWaitingPeriod(AtomicInteger waitingPeriod) {
		this.waitingPeriod = waitingPeriod;
	}

	public void  processClient() {  
    	
    	Client client;
    
    	client = clients.peek(); 
    	int time = client.getTservice();
    	
    	client.setTservice(client.getTservice()+1);//pentru ca scad inca de la inceput, inainte de a afisa. Nu modifica timpii
    
    	try {
    		
    	   for(int i = 1;i <= time;i++) {
			  
    			   if(client.getTservice() !=0 )
    				   client.decrementTime();
    			   else {
    				   Thread.sleep(1000);
    			   }
    			   
    			   Thread.sleep(990);
    			   waitingPeriod.decrementAndGet();		   
    				
    	   }   
    	}catch (InterruptedException e) {
     				e.printStackTrace();			
    	     }
    	      	   
    	
    	clients.remove(client);
    	
    	if(clients.isEmpty()) { // daca nu mai avem nimic in coada , oprim threadul 
    		this.stopRunning();
    	}

    }
	
	public void stopRunning() {
	    	running.set(false);
	    }
	    
	public void startRunning() {
	    	running.set(true);
	    }
 
    public AtomicBoolean getRunning() {
		return running;
	}

	public void setRunning(AtomicBoolean running) {
		this.running = running;
	}

	public boolean isOpen() {
		return running.get();
	}

	public String toString() {
    
		String c = "";
		if(!running.get())
    		return "Queue " + nr + ": closed.";
		c =  "Queue " + nr + ": ";
		for(Client client : clients)
			c += client+ " ";
    	return c;
    }
   
	public int getNr() {
		return nr;
	}

	public void setNr(int nr) {
		this.nr = nr;
	}

	public Queue<Client> getClients() {
		return clients;
	}

	public void setClients(LinkedBlockingQueue<Client> clients) {
		this.clients = clients;
	}

	
	
}
