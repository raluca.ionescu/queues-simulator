package PT2020.Assignment_2;

public class Client implements Comparable<Client> {

	private int id;
	private int tarrival;
	private int tservice;
	private int waiting; 
	private int finishtime;// cat timp a petercut la casa ( waitng + tservice)
	
	
	
	public Client(int i , int tarrival , int tservice) {
		id = i;
		this.tarrival = tarrival;
		this.tservice = tservice;
	
	}
	
	public int getFinishtime() {
		return finishtime;
	}
	
	public void incrementWaiting() {
		this.finishtime += 1;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setWaitingTime(int w) {
		this.waiting = w;
		finishtime = tservice + waiting;
	}
	
	public int getWaitingTime() {
		return finishtime;
	}
	public int getTarrival() {
		return tarrival;
	}

	public void setTarrival(int tarrival) {
		this.tarrival = tarrival;
	}

	public int getTservice() {
		return tservice;
	}
	
	public void decrementTime() {
		this.tservice --;
	}

	public void setTservice(int tservice) {
		this.tservice = tservice;
	}
	
	public String toString() {
		
		String mesaj = null;
		if(tservice == 0) {
			return mesaj;
		}
		
		return "("+ id+ ", "+ tarrival+ ", " + tservice + ")"; 
	}
	

	public int compareTo(Client c) {
		
		return this.tarrival - c.getTarrival();
		
	}

	
}
